import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

  inputText: string = '';
  dataType: string = '0';
  isTrue: string = 'false';

  constructor() { }

  ngOnInit(): void {
  }

  onChangeInput(event: any) {

    setTimeout(() => {
      if (this.inputText != '') {
        let number = parseInt(this.inputText);

        if (isNaN(number)) {
          this.inputText = '0';
        }
        else {
          if (number < 0) {
            this.inputText = '1';
            number = 1;
          }
          else {
            this.inputText = JSON.stringify(number)
          }

          if (this.dataType == '0') {
            this.isTrue = JSON.stringify(this.isPrime(number));
          }
          else {
            this.isTrue = JSON.stringify(this.isFibonacci(number));
          }
        }
      }
    }, 1000);
  }


  isPrime(number: number) {
    let prime = number != 1;

    for (let i = 2; i < number; i++) {
      if (number % i == 0) {
        return false;
        break;
      }
    }

    return prime;
  }

  isFibonacci(number: number) {
    if (this.isSquare(5 * (number * number) - 4) || this.isSquare(5 * (number * number) + 4)) {
      return true;
    }
    else {
      return false;
    }
  }

  isSquare(number: number) {
    return number > 0 && Math.sqrt(number) % 1 == 0;
  }
}
